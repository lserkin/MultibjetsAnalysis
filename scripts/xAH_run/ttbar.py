from xAH_config import xAH_config
c = xAH_config()

c.setalg("MainAnalysis", {"m_doPileup": 0,
                          "m_doSyst": 1,
                          "m_doTruth": 1,
                          "m_doNTUP": 1,
                          "m_doNTUPSyst": 1,
                          "m_doHistFitter": 1,
                          "m_doxAOD": 0,
                          "m_doRcJets": 1,
                          "m_doAK10Jets": 0,
                          "m_dobTagEff": False,
                          "m_doMM": False,
                          "m_dataSource": 1,
                          "m_debug": 0,
                          "m_verbose": 0,
                          "m_doTRF": 0,
                          "m_doTopPtCorrection": 2,
                          "m_doTtbarHFClassification": 3,
                          "m_smallR_etacut": 2.8})
