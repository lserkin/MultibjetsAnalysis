#ifndef MULTIB_HELPERFUNCTIONS_H
#define MULTIB_HELPERFUNCTIONS_H

#include <typeinfo>
#include <cxxabi.h>
// Gaudi/Athena include(s):
#include "AthContainers/normalizedTypeinfoName.h"

// local includes
#include "AsgTools/StatusCode.h"

// various xAOD includes
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"
#include "xAODMuon/Muon.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"


#include "EventPrimitives/EventPrimitivesHelpers.h"
#include "xAODPrimitives/IsolationType.h"
#include "xAODEgamma/EgammaxAODHelpers.h"


#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "xAODEventInfo/EventInfo.h"

#define MEV 1000

namespace HelperFunctions {

  // Deep copy helpers.
  template <typename T1, typename T2, typename T3>
  bool makeDeepCopy(xAOD::TStore* m_store, std::string containerName, const T1* cont){
    T1* cont_new = new T1;
    T2* auxcont_new = new T2;
    cont_new->setStore(auxcont_new);

    if(!m_store->record(cont_new, containerName).isSuccess()){ std::cout << "can't record " << containerName << std::endl; return false; }
    if(!m_store->record(auxcont_new, containerName+"Aux.").isSuccess()){  std::cout << "can't record aux" << containerName << std::endl;return false; }

    for(const auto p: *cont){
      T3* p_new = new T3;
      cont_new->push_back(p_new);
      *p_new = *p;
    }
    return true;
  }

  template <typename T1, typename T2>
  bool recordOutput(xAOD::TEvent* m_event, xAOD::TStore* m_store, std::string containerName){
    T1* cont(nullptr);
    T2* auxcont(nullptr);

    if(!m_store->retrieve(cont, containerName).isSuccess()) return false;
    if(!m_store->retrieve(auxcont, containerName+"Aux.").isSuccess()) return false;

    if(!m_event->record(cont, containerName).isSuccess()) return false;
    if(!m_event->record(auxcont, containerName+"Aux.").isSuccess()) return false;
    return true;
  }



  //Grab the track particle in a consistent way.

  inline const xAOD::TrackParticle* getTrackParticle(const xAOD::Electron* p){
    return p->trackParticle();
  }

  inline const xAOD::TrackParticle* getTrackParticle(const xAOD::Muon* p){
    const ElementLink<xAOD::TrackParticleContainer> tp_prime = p->primaryTrackParticleLink();
    if(!tp_prime.isValid()) return nullptr;
    return *tp_prime;
  }

  // d0 calculations: do it consistently for muon and electron
  template <typename T>
    float getD0sig(const T* p, const xAOD::EventInfo* info){
    auto track = getTrackParticle(p);
    if(!track) return -99.;

    double d0_sig = xAOD::TrackingHelpers::d0significance(track,
    							  info->beamPosSigmaX(),
    							  info->beamPosSigmaY(),
    							  info->beamPosSigmaXY());

    return d0_sig;
  }


  // z0 calculations: same
  template <typename T>
  float getZ0(const T* p, float primvertex_z){
    auto track = getTrackParticle(p);
    if(!track) return -99.;

    double z0 = track->z0() + track->vz() - primvertex_z;

    return z0;
  }


  //
  // For Sorting
  //
  struct pt_sort
  {

    inline bool operator() (const TLorentzVector& lhs, const TLorentzVector& rhs)
    {
      return (lhs.Pt() > rhs.Pt());
    }

    inline bool operator() (const TLorentzVector* lhs, const TLorentzVector* rhs)
    {
      return (lhs->Pt() > rhs->Pt());
    }

    inline bool operator() (const xAOD::IParticle& lhs, const xAOD::IParticle& rhs)
    {
      return (lhs.pt() > rhs.pt());
    }

    inline bool operator() (const xAOD::IParticle* lhs, const xAOD::IParticle* rhs)
    {
      return (lhs->pt() > rhs->pt());
    }
  };

  //Gets the number of primary vertices
  inline int getNVtx(xAOD::TEvent* m_event){
    int n_vtx = 0;
    const xAOD::VertexContainer* vertices(0);
    if ( m_event->retrieve( vertices, "PrimaryVertices" ).isSuccess() ) {
      for ( const auto& vx : *vertices ) {
        //if (vx->vertexType() == xAOD::VxType::PriVtx) {
        if (vx)n_vtx++;
        //}
      }
    } else {
      std::cout << "Failed to retrieve VertexContainer \"PrimaryVertices\", returning NULL" << std::endl;
      return -1;
    }
    return n_vtx;
  }

  template<typename T>
    T sort_container_pt(T* inCont){
    T sortedCont(SG::VIEW_ELEMENTS);
    for(auto el : *inCont) sortedCont.push_back( el );
    std::sort(sortedCont.begin(), sortedCont.end(), pt_sort());
    return sortedCont;
  }

  template<typename T>
    const T sort_container_pt(const T* inCont){
    ConstDataVector<T> sortedCont(SG::VIEW_ELEMENTS);

    for(auto el : *inCont) sortedCont.push_back( el );
    std::sort(sortedCont.begin(), sortedCont.end(), pt_sort());
    return *sortedCont.asDataVector();
  }

} // end namespace

#endif
