#ifndef MultibjetsAnalysis_JetHelper_h
#define MultibjetsAnalysis_JetHelper_h

#include <utility>

// xAOD
#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODRootAccess/TStore.h"

#include "MultibjetsAnalysis/HelperFunctions.h"

#include "JetSubStructureUtils/Nsubjettiness.h"
#include "JetSubStructureUtils/KtSplittingScale.h"

#include <memory>

// jet reclustering
#include <fastjet/JetDefinition.hh>

// top tagger
#include "BoostedJetTaggers/SubstructureTopTagger.h"

namespace ST{
class SUSYObjDef_xAOD;
}

class JetHelper
{

 public:

  JetHelper(ST::SUSYObjDef_xAOD*& susy_tools, xAOD::TEvent*& event, xAOD::TStore*& store);
  virtual ~JetHelper();

  void InitSubstructureCalculators(double radius);

  // Retreive the fat jet container
  void RetrieveFatJets(const std::string& fj_key,
        SubstructureTopTagger* top_tagger_smooth_tight = 0,
        SubstructureTopTagger* top_tagger_smooth_loose = 0,
        SubstructureTopTagger* top_tagger_lowpt_tight = 0,
        SubstructureTopTagger* top_tagger_lowpt_loose = 0,
        SubstructureTopTagger* top_tagger_highpt_tight = 0,
        SubstructureTopTagger* top_tagger_highpt_loose = 0);

  // Retreive the cluster container
  void RetrieveClusters(const std::string& clus_key);

  // Apply kinematic and quality cuts
  void DecorateBaselineJets(xAOD::JetContainer*& jets, std::string sys_name = "");
  void RetrieveSignalFatJets();
  void RetrieveFatJetsST(xAOD::JetContainer*& jets, xAOD::ShallowAuxContainer*& jets_aux);

  xAOD::JetContainer*    GetShallowCopy() {return m_jets_copy;};

  xAOD::CaloClusterContainer*    GetClustersShallowCopy() {return m_clus_copy;};

  // JetVector           GetSignalJets()  {return m_jets_vector;};
  UInt_t              GetNbBadJets()   {return m_nb_bad_jets;};
  // FatJetVector        GetSignalFatJets();



  const xAOD::JetContainer*            GetBaselineJets(bool is_nominal, bool affects_jets, std::string sys_name, xAOD::TStore*& m_store)   {
     const xAOD::JetContainer* baseline_jets(nullptr);
     std::string name = "BaselineJets";
     if(!is_nominal && affects_jets) name += sys_name;
     if(!m_store->retrieve( baseline_jets, name ).isSuccess()) std::cout << "major problem ! " << std::endl;
     return baseline_jets;
  };

  const xAOD::JetContainer*            GetSignalJets(bool is_nominal, bool affects_jets, std::string sys_name, xAOD::TStore*& m_store)   {
     const xAOD::JetContainer* signal_jets(nullptr);
     std::string name = "SignalJets";
     if(!is_nominal && affects_jets) name += sys_name;
     if(!m_store->retrieve( signal_jets, name ).isSuccess()) std::cout << "major problem ! " << std::endl;
     return signal_jets;
  };
  const xAOD::JetContainer*            GetSignalFatJets(){return m_fat_jets;};

  float GetBtaggingWeight();
  float GetJVTWeight();
  bool IsBtagged(const xAOD::Jet jet);

  inline void SetSmallREtaCut( const double eta ) { m_smallR_eta_cut = eta; }

 protected:

  JetSubStructureUtils::Nsubjettiness* m_tau1_calc;
  JetSubStructureUtils::Nsubjettiness* m_tau2_calc;
  JetSubStructureUtils::Nsubjettiness* m_tau3_calc;

  JetSubStructureUtils::KtSplittingScale* m_d12_calc;
  JetSubStructureUtils::KtSplittingScale* m_d23_calc;

   xAOD::TEvent*& m_event;  //!
   xAOD::TStore*& m_store;  //!

  // SUSYTools
  ST::SUSYObjDef_xAOD*& m_susy_tools ; //!

  // shallow copies
  xAOD::JetContainer*        m_jets_copy; //!
  xAOD::ShallowAuxContainer* m_jets_copy_aux; //!

  xAOD::JetContainer*        m_fj_copy; //!
  xAOD::ShallowAuxContainer* m_fj_copy_aux; //!

  xAOD::CaloClusterContainer*        m_clus_copy; //!
  xAOD::ShallowAuxContainer* m_clus_copy_aux; //!

  // jet list which satisfy the kinematic requirements applied to b-jets for the b-tagging event weight
  const xAOD::JetContainer* m_jets_btagging;

  // and the jet list for generic basline jets
  const xAOD::JetContainer* m_baseline_jets;

  // and the jet list for generic signal jets
  const xAOD::JetContainer* m_signal_jets;

  // the jet list for our standard fat jets as well
  const xAOD::JetContainer* m_fat_jets;

  // number of bad jets
  UInt_t m_nb_bad_jets;

  fastjet::ClusterSequence* m_cur_cs;

  double m_smallR_eta_cut;

};

#endif
