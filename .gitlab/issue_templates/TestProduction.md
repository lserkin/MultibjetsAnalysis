[//]: # (Please title your issue: "Test production for tag X.Y.Z-A-B")

# Tag X.Y.Z-A-B

Code is [here](https://gitlab.cern.ch/htx/MultibjetsAnalysis/tags/XXX)

## Questions About Production

Send an email to Loic <lvalery@cern.ch> or to the mailing list <atlas-phys-exotics-HtX-4tops@cern.ch>

## Related Issues:

- #AA

## Job Details

The jobs are ran with the following configurations:

:x: with systematics

:x: with output trees

:x: with data

There are XYZ samples for this production.

## Instructions

From your top level directory, do:

```python MultibjetsAnalysis/scripts/submit.py MultibjetsAnalysis/scripts/processes/test.list```

Be sure to provide ABCDEF the links to your panda pages for tracking.

## Progress

- [ ] Launch production
- [ ] Production finished
- [ ] Checks done

## Making Changes

Please edit the issue to make changes to any checkboxes and so forth.


## Checks

Please document changes in the comments of this issue. Feel free to attach any plots that could be useful to compare in the next steps.
